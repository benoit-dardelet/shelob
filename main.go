package main

import (
	"bytes"
	"errors"
	"log"
	"os"
	"path/filepath"
	"strconv"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/dialog"
	"fyne.io/fyne/v2/storage"
	"fyne.io/fyne/v2/widget"
	"github.com/beevik/etree"
	"github.com/goccy/go-graphviz"
	"github.com/goccy/go-graphviz/cgraph"
	"github.com/skratchdot/open-golang/open"
)

type PhoneExtraction struct {
	ID              string
	Name            string
	PathToSourceXML string
	AccountsID      []string
	ContactsID      []string
	Node            *cgraph.Node
}

type commonContact struct {
	SourceNode        *cgraph.Node
	CommonContactNode *cgraph.Node
	IsDiscovered      bool
}

var PhoneList []PhoneExtraction

var displayList *widget.List

var currentSelectedListRow int

var graph *cgraph.Graph

var g *graphviz.Graphviz

var grapherror error

var myApp fyne.App

var logger *log.Logger

func main() {

	// Creating the main graphviz instance
	g = graphviz.New()

	defer func() {
		g.Close()
	}()

	LoadLogger()

	myApp = app.NewWithID("SHELOB")
	myWindow := myApp.NewWindow("Shelob")

	selectXMLButton := GetSelectXMLButton(myWindow)
	removeXMLButton := GetRemoveXMLButton(myWindow)
	renderGraphButton := GetRenderGraphButton(myWindow)

	contentButton := container.NewVBox(
		selectXMLButton,
		removeXMLButton,
		renderGraphButton,
	)

	displayList = GetDisplayList()
	contentExpanding := container.NewVScroll(displayList)

	contentFinal := container.NewBorder(contentButton, nil, nil, nil, contentExpanding)

	myWindow.SetContent(contentFinal)
	myWindow.Resize(fyne.NewSize(1920, 1080))
	myWindow.ShowAndRun()

}

// Can't use default Go Marshalling because of the format of Cellebrite's XML
func parseXML(pathToFile string) (PhoneExtraction, error) {

	var phone PhoneExtraction

	logger.Print(pathToFile)

	doc := etree.NewDocument()
	if err := doc.ReadFromFile(pathToFile); err != nil {
		return phone, errors.New("this file is corrupted")
	}
	project := doc.SelectElement("project")

	// Adding case info to the struct
	phone.Name = project.SelectAttrValue("name", "No project name")
	phone.ID = project.SelectAttrValue("id", "No project id")
	phone.PathToSourceXML = pathToFile

	// Checking if not already selected and raising an error if so
	for _, checkingExtraction := range PhoneList {
		if checkingExtraction.ID == phone.ID {
			return phone, errors.New("project with the same id already ingested")
		}
	}

	// For each contact found add unique IDs
	for _, contact := range doc.FindElements("//model[@type='Contact']") {
		for _, contactID := range contact.FindElements("./multiModelField[@name='Entries']/model[@type='UserID']") {
			contactIDFound := contactID.FindElement("./field[@name='Value']").SelectElement("value")
			phone.ContactsID = append(phone.ContactsID, contactIDFound.Text())
		}
	}

	// TODO : Add parsing for phone user accounts

	// TODO : Add parsing for calls

	// TODO : Add parsing for messages

	// TODO : Add parsing for bluetooth connection

	// TODO : Add parsing for wifi hotspot and connection

	return phone, nil
}

// Find direct link and indirect link between PhoneExtraction struct in the list of all PhoneExtraction
func FindLink() {

	logger.Print("Looking for links")

	idAccountMap := make(map[string][]*cgraph.Node) // Tracking id to phone extraction
	idContactMap := make(map[string]commonContact)  // Tracking id to common contacts

	// For each phone extraction
	for index := range PhoneList {

		// Create a node to display on the graph
		nodeToAdd, err := graph.CreateNode(PhoneList[index].Name)
		if err != nil {
			logger.Fatalf("Error creating node for phone %s: %v", PhoneList[index].Name, err)
		}

		// Saving the node pointer to our phone object
		PhoneList[index].Node = nodeToAdd
		nodeToAdd.SetShape(cgraph.Shape("rectangle"))

		// For each Account ID add them to the map of
		for _, accountFromPhone := range PhoneList[index].AccountsID {

			idAccountMap[accountFromPhone] = append(idAccountMap[accountFromPhone], nodeToAdd)

			// TODO : Detect phones with the one or more account ID in common and display it on graph

		}

	}

	// Now that every phone account is loaded we can look for links with contacts
	for _, phone := range PhoneList {

		for _, contactFromPhone := range phone.ContactsID {

			if oneOrMultiplePhoneNodes, ok := idAccountMap[contactFromPhone]; ok {

				// If there is a match, add a link between the current phone and the matching phones

				for _, singleIDNode := range oneOrMultiplePhoneNodes {

					graph.CreateEdge(contactFromPhone, phone.Node, singleIDNode)

				}

			} else if commonContact, ok := idContactMap[contactFromPhone]; ok {

				// If there is two times the same contactID in the same phone we get out of here
				if commonContact.SourceNode == phone.Node {
					goto skipThisMatch
				}

				// If contact not discovered add link between source node and a new node
				if !commonContact.IsDiscovered {

					// Create node
					nodeToAdd, err := graph.CreateNode(contactFromPhone)
					if err != nil {
						logger.Fatal(err)
					}

					// Add node and change discovery value
					var tempContact = idContactMap[contactFromPhone]
					tempContact.IsDiscovered = true
					tempContact.CommonContactNode = nodeToAdd
					idContactMap[contactFromPhone] = tempContact

					// Finnaly add edge to the source phone node
					graph.CreateEdge(contactFromPhone, commonContact.SourceNode, nodeToAdd)

				}

				// Whatever happens add the edge between commonContact and the current phone
				graph.CreateEdge(contactFromPhone, phone.Node, idContactMap[contactFromPhone].CommonContactNode)

			} else {

				// Create a commonContact instance withtout node for the moment
				var newCommonContact = commonContact
				newCommonContact.SourceNode = phone.Node
				newCommonContact.IsDiscovered = false
				idContactMap[contactFromPhone] = newCommonContact

			}

		skipThisMatch:
		}

	}

}

// Takes graph struct, and saves it where it's told to
func RenderGraphAndSave(pathToSave string) {

	logger.Print("Rendering graph")

	// Set layout to circo, the best one so far
	graph.SetLayout("circo")

	// Render the graph
	var buf bytes.Buffer
	if err := g.Render(graph, "svg", &buf); err != nil {
		logger.Fatalf("%+v", err)
	}

	if err := os.WriteFile(pathToSave, buf.Bytes(), 0644); err != nil {
		logger.Fatal(err)
	}

	// Open the svg with the default application
	if err := open.Run(pathToSave); err != nil {
		logger.Print(err)
	}

}

func GetSelectXMLButton(myWindow fyne.Window) *widget.Button {

	button := widget.NewButton("Add XML file from your Inseyets's case", func() {

		logger.Print("Select XML button pressed")

		dialog.ShowFileOpen(func(FilePathReader fyne.URIReadCloser, err error) {
			if err != nil {
				return
			}
			if FilePathReader == nil {
				return
			}
			defer FilePathReader.Close()

			// Add the file to the list
			fileName := FilePathReader.URI().Path()
			newPhone, err := parseXML(fileName)
			if err != nil {
				dialog.ShowError(err, myWindow)
			} else {
				PhoneList = append(PhoneList, newPhone)

				// Refresh list
				displayList.Refresh()
			}

		}, myWindow)
	})
	return button
}

func GetRemoveXMLButton(myWindow fyne.Window) *widget.Button {

	button := widget.NewButton("Remove a phone extraction", func() {

		logger.Print("Remove XML button pressed")

		// Check if the row is still viable and if so delete it
		if currentSelectedListRow >= 0 && currentSelectedListRow < len(PhoneList) {
			// Remove the selected item
			PhoneList = append(PhoneList[:currentSelectedListRow], PhoneList[currentSelectedListRow+1:]...)
			// Refresh the list to reflect changes
			displayList.Refresh()
		} else {
			dialog.ShowError(errors.New("no row selected"), myWindow)
		}

	})

	return button
}

func GetRenderGraphButton(myWindow fyne.Window) *widget.Button {
	button := widget.NewButton("Render link graph", func() {

		logger.Print("Render graph button pressed")

		if len(PhoneList) == 1 {
			dialog.ShowError(errors.New("impossible to link only one file"), myWindow)
			return
		} else if len(PhoneList) == 0 {
			dialog.ShowError(errors.New("no files to link"), myWindow)
			return
		}

		fd := dialog.NewFileSave(func(FilePathWriter fyne.URIWriteCloser, err error) {
			if err != nil {
				return
			}
			if FilePathWriter == nil {
				return
			}
			defer FilePathWriter.Close()

			// Create graph instance
			graph, grapherror = g.Graph()
			if grapherror != nil {
				logger.Fatal(grapherror)
			}

			// We look for the links and populate the global graph variable
			FindLink()

			// We render the graph and save it to the desired path
			RenderGraphAndSave(FilePathWriter.URI().Path())

			// Delete graph instance
			if grapherror = graph.Close(); grapherror != nil {
				logger.Fatal(grapherror)
			}

			// Add used path as a preference
			myApp.Preferences().SetString("lastPathUsed", FilePathWriter.URI().Path())

		}, myWindow)

		lastPath := myApp.Preferences().StringWithFallback("lastPathUsed", "")

		if lastPath != "" {

			pathURI := storage.NewFileURI(filepath.Dir(lastPath))
			listableURI, err := storage.ListerForURI(pathURI)
			if err != nil {
				logger.Fatal(err)
			}
			fd.SetLocation(listableURI)

		}

		fd.Show()

	})
	button.Importance = widget.HighImportance
	return button
}

func GetDisplayList() *widget.List {

	listReturned := widget.NewList(
		func() int {
			// Set the number of items in the list
			return len(PhoneList)
		},
		func() fyne.CanvasObject {
			// Set the content of each item in the list
			return widget.NewLabel("")
		},
		func(index int, item fyne.CanvasObject) {
			// Set the content of each item based on the slice data
			item.(*widget.Label).SetText("Project : " + PhoneList[index].Name + " with ID : " + PhoneList[index].ID)
		},
	)

	// Initialize the value to a default
	currentSelectedListRow = -1

	// Update the selected item dynamically
	listReturned.OnSelected = func(id int) {
		currentSelectedListRow = id
		logger.Print("Row selected : " + strconv.Itoa(id))
	}

	return listReturned
}

// Initialize the logger with the corect path depending on OS
func LoadLogger() {

	file, err := os.OpenFile("log.txt", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0644)
	if err != nil {
		log.Panicf("Failed to open log file: %v", err)
	}

	// Create a new logger writing to the file
	logger = log.New(file, "Shelob: ", log.LstdFlags|log.Lshortfile)

	logger.Print("Logger initialized !")

}
